#!/usr/bin/env python3
import gi
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Handler:
    def onDestroy(self, *args):
        Gtk.main_quit()

    def onConnectClicked(self, button):
        global ip
        global flag1
        global flag2
        print("Connect button clicked!")
        os.system("gzdoom -join " + ip +  " -iwad " + wad + " +dmflags " + flag1 + " +dmflags2 " + flag2)

    def onIPEdited(self, entry):
        global ip
        ip = entry.get_text()
        print(ip)

    def onFlag1Edited(self, flag1entry):
        global flag1
        flag1 = flag1entry.get_text()
        print(flag1)

    def onFlag2Edited(self, flag2entry):
        global flag2
        flag2 = flag2entry.get_text()
        print(flag2)

    def onWadSelected(self, wadselection):
        global wad
        wad = wadselection.get_filename()
        print(wad)
builder = Gtk.Builder()
builder.add_from_file("window.glade")
builder.connect_signals(Handler())

window = builder.get_object("main_window")
window.show_all()

Gtk.main()
