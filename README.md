# GZDoom Connect

A simple GTK based interface for configuring and starting multiplayer GZDoom games

## Currently Working

* Setting IP to connect to
* Setting dmflags
* Setting WAD file to use

## Planned Features

* Allow adding mod files
* Proper packaging (Most likely Flatpak)
* Windows port